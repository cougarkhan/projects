param([switch]$2003)

$user = get-credential

if ($2003) {
	schtasks.exe /CREATE /RU $user.UserName /RP /TN "SQL Maintenance" /SC DAILY /TR "C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe -file \\bis-fs1-prd\sql_maintenance\sql_maintenance.ps1" /ST 18:30 /F
} else {
	schtasks.exe /CREATE /XML "\\bis-fs1-prd.ead.ubc.ca\sql_maintenance\etc\SQL Maintenance.xml" /TN "SQL Maintenance" /RU $user.UserName /RP /F /S (gci env:COMPUTERNAME).value
}