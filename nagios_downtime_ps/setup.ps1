# Disable IEES for Admins
# Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
# & rundll32 iesetup.dll,IEHardenAdmin
# & rundll32 iesetup.dll,IEHardenMachineNow

$scriptpath = $MyInvocation.MyCommand.Path
$scriptpath = Split-Path $scriptpath

try {
	$rsa_exists = [bool](gp REGISTRY::HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\SshHostKeys -Name rsa2@22:bis-nagios.bussops.ubc.ca -EA stop)
}
catch [System.Exception] {
	$rsa_exists = $false
}

if ($rsa_exists -eq $false) {
	$arguments = @("-l","nagremote","-i","$scriptpath\remote.ppk","bis-nagios.bussops.ubc.ca","exit")
	start-process -filepath "$scriptpath\plink.exe" -argumentlist $arguments -wait -nonewwindow
}

if ((gwmi win32_operatingsystem).version -ge 6) {
	if ((test-path "C:\Users\Public\Desktop\15 Minute Downtime.lnk") -eq $false) {
		cp "$scriptpath\15 Minute Downtime.lnk" "C:\Users\Public\Desktop\15 Minute Downtime.lnk"
	}
} else {
	if ((test-path "C:\Documents and Settings\All Users\Desktop\15 Minute Downtime.lnk") -eq $false) {
		cp "$scriptpath\15 Minute Downtime.lnk" "C:\Documents and Settings\All Users\Desktop\15 Minute Downtime.lnk"
	}
}



# Disable IEES for Users
# Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
# & rundll32 iesetup.dll,IEHardenUser