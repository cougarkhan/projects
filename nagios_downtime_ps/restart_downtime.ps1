try {
	$rsa_exists = [bool](gp REGISTRY::HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\SshHostKeys -Name rsa2@22:bis-nagios.bussops.ubc.ca -EA stop)
}
catch [System.Exception] {
	$rsa_exists = $false
}

if ($rsa_exists -eq $false) {
	$arguments = @("-l","nagremote","-i",".\remote.ppk","bis-nagios.bussops.ubc.ca","exit")
	start-process -filepath ".\plink.exe" -argumentlist $arguments -wait -nonewwindow
}

& C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -file \\bussops.ubc.ca\SYSVOL\bussops.ubc.ca\scripts\Nagios\schedule_downtime.ps1 -l -d 900 -c "Restart"