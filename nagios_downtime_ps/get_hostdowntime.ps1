#$host_name = ((gci env:COMPUTERNAME).value).toupper()
$host_name = "BIS-BACKUP1"

$arguments_host = @("-l",
				"nagremote",
				"-i",
				".\remote.ppk",
				"bis-nagios.bussops.ubc.ca",
				"-batch",
				"grep -A 11 -i $([char]92)$([char]34)hostdowntime$([char]92)$([char]34) /usr/local/nagios/var/status.dat")

$arguments_service = @("-l",
				"nagremote",
				"-i",
				".\remote.ppk",
				"bis-nagios.bussops.ubc.ca",
				"-batch",
				"grep -A 12 -i $([char]92)$([char]34)servicedowntime$([char]92)$([char]34) /usr/local/nagios/var/status.dat")

start-process -filepath ".\plink.exe" -argumentlist $arguments_host -wait -nonewwindow -RedirectStandardOutput ".\temp_host.txt"
start-process -filepath ".\plink.exe" -argumentlist $arguments_service -wait -nonewwindow -RedirectStandardOutput ".\temp_service.txt"

$host_downtime_results = gc ".\temp_host.txt"
rm ".\temp_host.txt"
$service_downtime_results = gc ".\temp_service.txt"
rm ".\temp_service.txt"

for($i=0; $i -lt $host_downtime_results.count; $i++) {
	if($host_downtime_results[$i].trim().startswith("hostdowntime") -AND $host_downtime_results[$i+1].trim().startswith("host_name") -AND $host_downtime_results[$i+1].contains($host_name)) {
		
		$object_host = new-object system.object
		
		for($x=$i+1;($host_downtime_results[$x].contains("}")) -ne $true; $x++) {
			$values = $host_downtime_results[$x].trim().split("=")
			$object_host | Add-member -type NoteProperty -name $values[0] -value $values[1]
			$i=$x
		}
		$entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.entry_time))

		if (($(date)-$entry_time).totalseconds -lt 20) {
			write-host "Entry Found" -foregroundcolor GREEN
			write-host "Host Downtime has been set for $host_name" -foregroundcolor GREEN
			$object_host.entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.entry_time))
			$object_host.start_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.start_time))
			$object_host.end_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.end_time))
			$object_host
		} else {
			write-host "Found a previousley scheduled host downtime for this host." -foregroundcolor RED
		}
		
		$object_host = $null
	}
}

for($i=0; $i -lt $service_downtime_results.count; $i++) {
	if($service_downtime_results[$i].trim().startswith("servicedowntime") -AND $service_downtime_results[$i+1].trim().startswith("host_name") -AND $service_downtime_results[$i+1].contains($host_name)) {
		
		$object_service = new-object system.object
		
		for($x=$i+1;($service_downtime_results[$x].contains("}")) -ne $true; $x++) {
			$values = $service_downtime_results[$x].trim().split("=")
			$object_service | Add-member -type NoteProperty -name $values[0] -value $values[1]
			$i=$x
		}
		$entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.entry_time))

		if (($(date)-$entry_time).totalseconds -lt 800) {
			write-host "Entry Found" -foregroundcolor GREEN
			write-host "Service Downtime has been set for $host_name" -foregroundcolor GREEN
			$object_service.entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.entry_time))
			$object_service.start_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.start_time))
			$object_service.end_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.end_time))
			$object_service
		} else {
			write-host "Found a previousley scheduled service downtime for this host." -foregroundcolor RED
		}
		
		$object_service = $null
	}
}