param (
		[alias("h")]
		[string]$hostname,
		[alias("l")]
		[switch]$localhost,
		[alias("s")]
		[switch]$services,
		[alias("d")]
		[string]$duration="900", 
		[alias("c")]
		[string]$comment="UNDEFINED",
		[alias("i")]
		[string]$inputFile=".\hosts.txt")

$author = (gci env:USERNAME).value

if ($services) {
	$type = 2
} else {
	$type = 1
}

if ($localhost -eq $true) {
	$machines = (gci env:COMPUTERNAME).value
} else {
	if (!$hostname) {
		if(!(test-path $inputFile)) {
			Write-host "$([char]34)$inputFile$([char]34) does not exist!"
			exit
		} else {
			$machines = gc $inputFile
			if ($machines -eq $null) {
				Write-Host "No hosts specified at command line or within hosts.txt!"
				exit
			}
		}
	} else {
		$machines = $hostname
	}
}

$machines | %{ 
	$machine_name = $_.ToUpper()
	$arguments = @("-l",
					"nagremote",
					"-i",
					".\remote.ppk",
					"bis-nagios.bussops.ubc.ca",
					"-batch",
					"/home/nagremote/nagios-downtimes/host-downtime.sh",
					"$type",
					"$machine_name",
					"$duration",
					"$author",
					"$([char]92)$([char]34)$comment$([char]92)$([char]34)")
	start-process -filepath ".\plink.exe" -argumentlist $arguments -nonewwindow -wait
}




