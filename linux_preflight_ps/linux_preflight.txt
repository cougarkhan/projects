sed -i s/^server/#server/ ntp.conf
sed -i "/^server ntp2.ubc.ca/a server 137.82.70.247\nserver 137.82.70.248\nserver 142.103.216.33\nserver 142.103.216.34\n" ntp.conf

sudo sed -i s/^nameserver/#nameserver/ /etc/resolv.conf
sudo sh -c 'echo "nameserver 142.103.178.117" >> /etc/resolv.conf'


sudo sed -i s/^bind_ip/#bind_ip/ /etc/mongod.conf
echo "configsvr=true" | sudo tee -a /etc/mongod.conf
sudo tail -f /var/log/mongodb/mongod.log
echo "configdb=mpal-rhel6-5:27019,mpal-rhel6-6:27019,mpal-rhel6-7:27019" | sudo tee -a /etc/mongod.conf
sudo mongos --configdb mpal-rhel6-5:27019,mpal-rhel6-6:27019,mpal-rhel6-7:27019
echo "shardsvr=true" | sudo tee -a /etc/mongod.conf