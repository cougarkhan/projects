##############################################################################################
#
# Script to restart user space application when it hangs.
#
##############################################################################################
#
# This script will check 1 condition.
# 
# 1. It will check whether $filename is older than $fileage in minutes..  
#    If the condition is true then an alert will be sent.
#
##############################################################################################
#
# Script written by Michael Pal on Feb 17th 2012
#
##############################################################################################

[CmdletBinding()] param (
						[Parameter(Position=0,mandatory=$false)][alias("fn")][string]$fileName,
						[Parameter(Position=1,mandatory=$false)][alias("fa")][string]$fileAge,
						[alias("h")][switch]$help
						)
# Import send email and date time functions
 . .\functions\format_date.ps1
 
function global:fun_help() {
$helpText = @"

NAME: check_file_age.ps1

This script will check if -filename exists.  If it does 
and the file is older than -fileage in minutes then an alert will be sent 
out.

PARAMETERS:

-fn`t-filename`tName of the file to look for.
-fa`t-fileage`tHow old the file can be in minutes before an alert is sent out.
-h`t-help`t`tShow this help menu.
`n 

"@
$helpText
exit
}

if ($help) { fun_help }
if (!$fileName) { fun_help }
if (!$fileAge) { fun_help }

$email = [xml](gc .\etc\emailconfig.xml)
[string[]]$log = @()
$fileList = ls $filename
$errorCondition = $null


if ($fileList.count -lt 1) {
	write-host "Nothing to see here folks"
	exit
} else {
	foreach ($file in $fileList) {
		$fileMinutes = $file.creationtime.timeofday.totalminutes
		$currentMinutes = (date).timeofday.totalminutes
		
		if (($currentMinutes - $fileMinutes) -gt $fileAge) {
			$errorCondition = $true
			$log += $file
		}
	}
}

if ($errorCondition -eq $true) {
	$server = (gci env:computername).value
	[string]$subject = "$filename is stuck on $server."
	[string]$from = $email.email.sender
	[string[]]$to = $email.email.recipients
	[string]$body = ($log) | out-string

	send-mailmessage -from $from -to $to -subject $subject -body $body -smtpServer $email.email.smtpServer
}

