﻿function send_email([string]$err_msg="There was an error.",[string]$to) {
    # SMTP Server
    $smtpServer = "mail-relay.ubc.ca"

    # Create new SMTP object to send email
    $smtp = new-object Net.Mail.SmtpClient($smtpServer)

    # Send the Email
    $from = $Env:COMPUTERNAME + "@bussops.ubc.ca"
    $subject = "Errors on " + $Env:COMPUTERNAME 
    $body = "$err_msg`n`n"

    $smtp.Send($from, $to, $subject, $body)
}