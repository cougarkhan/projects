﻿##############################################################################################
#
# Script to restart DbSql.exe and DsxPW.exe when DbSql.exe hangs.
#
##############################################################################################
#
# This script will check 1 condition.
# 
# 1. It will check whether the process named DbSql.exe has stopped responding.  
#    If it has then it will kill dbsql.exe and dsxpw.exe and restart dbsql.exe which will 
#    automatically restart dsxpw.exe.
#
#
##############################################################################################
#
# Script written by Michael Pal on Nov 2nd 2011
#
##############################################################################################

# Import send email and date time functions
  . .\functions\format_date.ps1

# Array Variable for storing error message
[array]$errorLog = @{}

#Send Email Settings
$email = [xml](gc .\etc\emailconfig.xml)

# funciton to kill processes
function killProcess ($process) {
	if ($process) {
		$process.kill()
	}
}

# Get process handles for DbSql.exe and DsxPW.exe
$dbsql = gps dbsql -ErrorAction SilentlyContinue -ErrorVariable $dbsqlErr
$dsxpw = gps dsxpw -ErrorAction SilentlyContinue -ErrorVariable $dsxpwErr


# If the Responding property for DbSql.exe is $false then kill the processes and restart them
if ($dbsql.Responding -eq $false) {
	$errDate = Get-Date
	$errorLog = "Process DbSql.exe has stopped responding at approximately $errDate`n"
	
	killProcess($dsxpw)
	$errorLog += "`nKilled Process DsxPW.exe at " + (Get-Date).toshorttimestring() +"`n"
	
	killProcess($dbsql)
	$errorLog += "`nKilled Process DbSql.exe at " + (Get-Date).toshorttimestring() +"`n"
	
	sleep 5
	& "D:\WinDSX\DbSql.exe" "-DsxOp", "mpal", "-DsxPW", "good2go"
	$errorLog += "`nRestarted Process DbSql.exe at " + (Get-Date).toshorttimestring() +"`n"
	$errorLog += "`nGood Hunting!"
	
	$server = (gci env:computername).value
	[string]$subject = "Errors on $server."
	[string]$from = $email.email.sender
	[string[]]$to = $email.email.recipients
	[string]$body = ($errorLog) | out-string

	send-mailmessage -from $from -to $to -subject $subject -body $body -smtpServer $email.email.smtpServer
}