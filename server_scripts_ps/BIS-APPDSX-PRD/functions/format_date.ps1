﻿function format_date {
	# Format Date Strings

	return Get-Date -Format yyyyMMdd
}

function format_time {
	#Format Time Strings
	
	return Get-Date -Format HHmmss
}

function log_time {
	return Get-Date -Format HH:mm:ss
}

function create_log {
	param ([string]$logPath)
	
	if (!(Test-Path $logPath)) {
		Write-Host "Creating new log at $logPath"
		$log = New-Item -Path $logPath -type File -Force
	} else {
		Write-Host "Retrieving log file " -nonewline
		Write-Host "$logPath" -nonewline -foregroundcolor "CYAN"
		Write-Host "..." -nonewline
		
		$log = (gi $logPath)
	}
	
	return $log
}