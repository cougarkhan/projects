function kill_process {
	param ($process, $error_log_file)
	$log_date = date -format yyyyMMdd-HHmmss
	$error_log_file2 = "$error_log_file.$log_date"
	
	$process_name = $process.name
	Write-Host (log_time)"Killing Process $process_name..."
	$process.refresh()
	if ($process.hasexited -eq $false) {
		$process.kill()
		start-sleep 2
		Write-Host (log_time)"Sent Kill signal to $process_name."
		if (test-path $error_log_file) {
			mv $error_log_file $error_log_file2
			Write-Host (log_time)"Renamed $error_log_file to $error_log_file2"
		}
		$process.refresh()
		if ($process.hasexited -eq $true) {
			Write-Host (log_time)"Process $process_name has exited."
			$process.close()
		}
	}
	if (($process_check = get-process $process_name -ea SILENTLYCONTINUE) -eq $null) {
		Write-Host (log_time)"Process $process_name Killed successfully."
		return $true
	} else {
		Write-Host (log_time)"Process $process_name still exists.  Process Kill Failed."
		return $false
	}
}

function start_process {
	param ($file_path, [string]$process_name, [string[]]$arguments)
	
	Write-Host (log_time)"Starting Process $process_name..."
	start-process -filepath $file_path -argumentlist $arguments
	start-sleep 2
	if (($process_check = get-process $process_name -ea SILENTLYCONTINUE) -ne $null) {
		Write-Host (log_time)"Process $process_name Started successfully."
		$process_check
		return $true
	} else {
		Write-Host (log_time)"Process $process_name does not exist.  Process Start Failed."
		return $false
	}
}

function restart_process {
	param ($process, [string[]]$arguments)
	
	$process_name = $process.name
	$process_id = $process.id
	$process_file_path = $process.path
	
	Write-Host (log_time)"Restarting Process $process_name..."
	
	if ((kill_process $process $error_log_file) -eq $true) {
		if ((start_process $process_file_path $process_name $arguments) -eq $true) {
			$process_check_id = (get-process $process_name -ea SILENTLYCONTINUE).id
			if ($process_id -ne $process_check_id) {
				Write-Host (log_time)"Process $process_name was Restarted successfully."
				return $true
			} else {
				Write-Host (log_time)"Process $process_name ID is the same as the previous killed process.  Something has Failed."
				Write-Host (log_time)"Old PID : $process_id"
				Write-Host (log_time)"Old PID : $process_check_id"
				return $false
			}
		} else {
			return $false
		}
	} else {
		return $false
	}
}

function check_error_log {
	param ([string]$error_log_file)
	
	if (test-path $error_log_file) {
		$cs = gc $error_log_file -tail 11

		$code = "Code = 80004005"
		$source = "Source = Microsoft OLE DB Provider for SQL Server"
		$desc = "Desc = Connection failure"

		for($i=0;$i -lt $cs.length;$i++) {
			
			if ($cs[$i] -eq $code) {
				if ($cs[$i+2] -eq $source -AND $cs[$i+3] -eq $desc) {
					$date_line = $cs[$i-1].split(" ")
					$d1 = date -format MM/dd/yy
					$temp = $date_line[1].split("/")
					$temp2 = $temp[0]+"/"+$temp[1]+"/"+$temp[2]
					$d2 = date $temp2 -format MM/dd/yy
					if ($d1 -eq $d2) {
						Write-Host (log_time)"Errors in CsErrorLog.txt."
						return $true
					} else {
						return $false
					}
				}
			}
		}
	}
}