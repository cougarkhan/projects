. .\functions\format_date.ps1
. .\functions\dsx_functions.ps1
. .\functions\network_functions.ps1

$logDate = format_date
$logTime = format_time
$email = [xml](gc .\etc\emailconfig.xml)
$config = [xml](gc .\etc\check_cs_config.xml)
$process_config = $config.config.process
$log_config = $config.config.log
[string[]]$arguments = $process_config.arguments
$process_name = $process_config.name
$process_file_path = $process_config.path
$error_log_file = $log_config.error_log_file
$check_failure = $null
$server = (gci env:computername).value
$log = create_log "C:\Temp\$server-$logTime-$logDate.txt"
$process_object = get-process $process_name -ea SILENTLYCONTINUE

Start-Transcript -path $log
Write-Host "`n"

# Process not running
if ($process_object -eq $null -AND $check_failure -eq $null) {
	$check_failure = "Process not running"
	Write-Host (log_time)"$check_failure"
	$result = start_process $process_file_path $process_name $arguments
}

# Process runnning but not responding
if ($process_object -ne $null -AND $process_object.Responding -eq $false -AND $check_failure -eq $null) {
	$check_failure = "Process runnning but not responding"
	Write-Host (log_time)"$check_failure"
	$result = restart_process $process_object $arguments
}

# Process running but SQL errors found in ErrorLog
if ($process_object -ne $null -AND $check_failure -eq $null -AND (check_error_log $error_log_file) -eq $true) {
	$check_failure = "Process running but SQL errors found in ErrorLog"
	Write-Host (log_time)"$check_failure"
	$result = restart_process $process_object $arguments
}

# Process running but Microsoft Visual C++ Runtime Library Error has occured
if ($process_object -ne $null -AND $process_object.MainWindowTitle -eq "Microsoft Visual C++ Runtime Library" -AND $check_failure -eq $null) {
	$check_failure = "Process running but Microsoft Visual C++ Runtime Library Error has occured"
	Write-Host (log_time)"$check_failure"
	$result = restart_process $process_object $arguments
}

# Process running but port 22223 not responding
if ($process_object -ne $null -AND (Test-Port -comp bis-appdsx-prd -port 22223 -tcp).open -eq $false -AND $check_failure -eq $null) {
	$check_failure = "Process running but port 22223 not responding"
	Write-Host (log_time)"$check_failure"
	$result = restart_process $process_object $arguments
}

Write-Host "`n"
Stop-Transcript

if ($check_failure -ne $null) {

	[string]$from = $email.email.sender
	[string[]]$to = $email.email.recipients
	[string]$subject = "Errors on $Server. $process_name Failed!. $check_failure"
	[string]$body = (gc $log) | out-string

	send-mailmessage -from $from -to $to -subject $subject -body $body -smtpServer $email.email.smtpServer
} else {
	rm $log -force
}