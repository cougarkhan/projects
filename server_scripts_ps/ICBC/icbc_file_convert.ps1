#############################################################################
#	Create text file from xls file.  This file is uploaded to ICBC.
#	The values within this file have specific positions.
#
#	Written by Michael Pal Oct 25th 2012
#
#############################################################################

#############################################################################
# Global Values
#############################################################################
$scriptpath = $MyInvocation.MyCommand.Path
$scriptpath = Split-Path $scriptpath
$file_csv = "$scriptpath\icbc.csv"
$file_icbc = "$scriptpath\ICBC.LST"
$file_winscp = "$scriptpath\script.txt"
$csv_headers = "State","H1","License","H2","Type","H3","H4","VIN","H5","H6","H7","H8","H9","H10","UBCO"

function Release-Ref ($ref) {
	([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$ref) -gt 0)
	[System.GC]::Collect()
	[System.GC]::WaitForPendingFinalizers()
}

#############################################################################
# Import License Plates from excel file
#############################################################################

if ($file_excel = (ls ".\view.xls").fullname) {
	$existing_excel_instances = @(Get-Process [e]xcel | %{$_.Id})
	$object_excel = new-object -comobject excel.application
	$object_excel_id = Get-Process excel | %{$_.Id} | ?{$existing_excel_instances -notcontains $_}
	$object_excel.visible = $false
	write-host "Opening Excel File $file_excel..." -nonewline
	if ($object_workbook = $object_excel.workbooks.open($file_excel)) {
		write-host "Completed"
	} else {
		write-host "Failed."
		read-host "`nPress Enter to exit...."
		exit
	}
	write-host "Modifying Output..."
	$object_worksheet = $object_workbook.worksheets.item(1)
	
	for ($row=1; $row -lt 6; $row++) {
		$result = $object_worksheet.rows.item(1).Delete() | out-null
	}
	$object_worksheet.rows.item(2).Delete() | out-null

	
	write-host "Saving CSV File..." -nonewline
	$object_workbook.SaveAs($file_csv, 6)
	if (test-path $file_csv) {
		write-host "Compelted"
	} else {
		write-host "Failed."
		read-host "`nPress Enter to exit...."
		Stop-Process -Id $object_excel_id -Force -ErrorAction SilentlyContinue
		$result = Release-Ref $object_worksheet
		$result = Release-Ref $object_workbook
		$result = Release-Ref $object_excel
		exit
	}
	write-host "Closing Excel File..."
	
	Stop-Process -Id $object_excel_id -Force -ErrorAction SilentlyContinue
	$result = Release-Ref $object_worksheet
	$result = Release-Ref $object_workbook
	$result = Release-Ref $object_excel
	
	$license_plates = import-csv $file_csv -header $csv_headers
	rm $file_csv -force
	
} else {
	write-host "Excel File not found!"
	read-host "`nPress Enter to exit...."
	exit
}

#############################################################################
# Begin Fields Values
#############################################################################

write-host "Creating ICBC.LST file..."
$result = new-item $file_icbc -type file -force

foreach ($plate in $license_plates) {
	if ($plate.License -ne "" -AND $plate.UBCO -eq "0.00") {
																				# Start Position  End Position  Length  Values  					Mandatory/Optional
		[string]$transaction_code = "370"										# 1 				3 			3 		370 - vehicle search 		mandatory
		[string]$search_list_number = "00"										# 4 				5 			2 		any alpha numeric value 	mandatory
		[string]$search_line_number = "00"										# 6 				7 			2 		any alpha numeric value 	mandatory
		[string]$status_request_type = "S"										# 8 				8 			1 		S - name and address 		mandatory
																				#										N - name
																				#										A - address
		[string]$filler1 = "             "										# 9 				21 			13 		Blank						mandatory
		[string]$vehicle_registration_Number = ""								# 14 				21 			8 									Either registration OR
		[string]$policy_number = $plate.License									# 22 				27 			6 									policy number must be entered
		[string]$filler2 = "       "											# 28 				34 			7 		Blank
		[string]$canadian_custom_code = " "										# 35 				35 			1 		R�transfer of ownership		Optional
																				#										processed�permanent
																				#										customers restrictions on
																				#										vehicle
																				#										T�transfer of ownership
																				#										processed�temporary
																				#										customs restrictions on
																				#										vehicle
																				#										P�permanent restriction
																				#										placed against the record
																				#										for three years
																				#										L�temporary restriction
																				#										placed against the record
																				#										(blank if not applicable)
		[string]$comment = "                   "								# 36 				54 			19 									Optional
		[string]$filler3 = "                          "							# 55 				80 			26 		Blank
		
		
		#############################################################################
		# Populate content for file
		#############################################################################
		
		[string]$output = $transaction_code+$search_list_number+$search_line_number+$status_request_type+$filler1+$vehicle_registration_Number+$policy_number+$filler2+$canadian_custom_code+$comment+$filler3
		$output | ac $file_icbc
		
		# DEBUGGING ###################
		# write-host "`n"
		# write-host "transaction_code : $transaction_code`tLength : "$transaction_code.length
		# write-host "search_list_number : $search_list_number`tLength : "$search_list_number.length
		# write-host "search_line_number : $search_line_number`tLength : "$search_line_number.length
		# write-host "status_request_type : $status_request_type`tLength : "$status_request_type.length
		# write-host "filler1 : $filler1`tLength : "$filler1.length
		# write-host "vehicle_registration_Number : $vehicle_registration_Number`tLength : "$vehicle_registration_Number.length
		# write-host "policy_number : $policy_number`tLength : "$policy_number.length
		# write-host "filler2 : $filler2`tLength : "$filler2.length
		# write-host "canadian_custom_code : $canadian_custom_code`tLength : "$canadian_custom_code.length
		# write-host "comment : $comment`tLength : "$comment.length
		# write-host "filler3 : $filler3`tLength : "$filler3.length
		# write-host "`n"
		# DEBUGGING ###################
		
		}
}

#############################################################################
# Upload File
#############################################################################

write-host "Creating Script File..."
"put $file_icbc" | sc $file_winscp
"close" | ac $file_winscp
"exit" | ac $file_winscp

if (test-path $file_winscp) {
	$arguments = @("icbc","/script=$file_winscp")
	write-host "Begining ICBC.LST file upload to FTP2.ICBC.COM."
	start-process -filepath "$scriptpath\WinSCP.com" -argumentlist $arguments -nonewwindow -wait
	rm $file_winscp
} else {
	write-host "No WinSCP script file found in current directory."
	exit
}
$today = date -format yyyyMMdd-HHmmss
$folder_archive = "$scriptpath\Archive\$today"
$result = mkdir $folder_archive
write-host "Archiving view.xls and ICBC.LST to .\Archive\$d"
mv $file_icbc $folder_archive -force
mv $file_excel $folder_archive -force
if (test-path "$folder_archive\view.xls") { write-host "view.xls Archive Exists" } else { write-host "view.xls Does NOT Archive Exists" }
if (test-path "$folder_archive\ICBC.LST") { write-host "ICBC.LST Archive Exists" } else { write-host "ICBC.LST Does NOT Archive Exists" }
read-host "`nPress Enter to exit...."