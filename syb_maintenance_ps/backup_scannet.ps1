#####################################################################################
#
# Version 1.01
#
# Written on 07/08/2012
# By Michael Pal
# 
#####################################################################################
param([string]$configFiles = ".\etc\runtime.txt")

foreach ($configFile in (gc $configFiles)) {

	if ($configFile -eq "") {
		$errorResults = @{"Config File $configFile does not exist.  Exiting..." = $false}
		$terminate = 1
	}
	if (test-path ".\etc\$configFile") {
		$configFile = ".\etc\$configFile"
		write-host "Found Config file in $configFile"
	} elseif (test-path $configFile) {
		write-host "Found Config file in $configFile"
	} else {
		$errorResults = @{"Load Config File $configFile..." = $false}
		$terminate = 1
	}

	. .\functions\format_date.ps1
	. .\functions\db_functions.ps1
	. .\functions\auth_functions.ps1
	. .\functions\syb_db_functions.ps1

	$logDate = format_date
	$logTime = format_time
	$email = [xml](gc .\etc\emailconfig.xml)
	if ($terminate -ne 1) {
		$settings = [xml](gc $configFile)
		if ($settings.config.server) {
			$remoteServer = $settings.config.server.servername
			$remoteService = $settings.config.server.servicename
		}
		$server = (gci env:computername).value
		$mappedDrives = $settings.config.mappedDrives.drive
		$options = $settings.config.options
		$workingFolder = $options.workingFolder
		$passwordFile = $options.pwFile
		$keyFile = $options.keyFile
		$sftp = $settings.config.sftp
		$hostname = $sftp.hostname
		$certfile = $sftp.certfile
		$username = $sftp.username
		$db = $settings.config.backups.db
		$dbName = $db.name
		$adminName = $db.admin
		$truncateEXE = $options.truncateEXE
		$errorResults = @{}
		[string[]]$scannetArgs = @()
	}
	$log = create_log "\\bis-fs1-prd.ead.ubc.ca\Logs\JobLogs\$logDate-$logTime-$server-$dbName.txt"

	Start-Transcript -Path $log
	
	# if ((test-path $workingFolder) -eq $false) {
		# new-item $workingfolder -t d
	# }
	
	Trap {
		$err = $_.Exception
		while ( $err.InnerException )
			{
			$err = $err.InnerException
			write-output $err.Message
		};
		continue
	}

	if ($terminate -ne 1) {
		Write-Host "`n--------------------------------------------"
		Write-Host " Mapping Drives..."
		Write-Host "--------------------------------------------`n"
		# Map drives for operations
		$mappedDrives | %{ 
			if ((mapOpDrive $_.root $_.name) -eq $false) {
				$dRoot = $_.root
				$dName = $_.name
				$errorResults.Add("Mapping $dRoot to $dName", $false)
				$terminate = 1
			}
		}
	}

	if ($terminate -ne 1) {
		if ($db.backup -eq 1) {
			Write-Host "`n--------------------------------------------"
			Write-Host " Backup Server Operations..."
			Write-Host "--------------------------------------------`n"
			if (($backupName = backupScanNetDB $passwordFile $keyFile $workingFolder $dbName $options.backupEXE) -eq $false) {
				$errorResults.Add("Backup $dbName",$false)
				$terminate = 1
			}
			if ($terminate -ne 1) {
				Write-Host "`n--------------------------------------------"
				Write-Host " Truncate Log Operations..."
				Write-Host "--------------------------------------------`n"
				truncate_log $passwordFile $keyFile $adminName $dbName $truncateEXE
			}
		}
	}

	if ($terminate -ne 1) {
		if ($db.compress -eq 1) {
			Write-Host "`n--------------------------------------------"
			Write-Host " Compress File Operation..."
			Write-Host "--------------------------------------------`n"
			if (($compressedFile = gi (compress $backupName.fullname $options.sevenZip)) -eq $false) {
				$bkName = $backupName.name
				$errorResults.Add("Compress $bkName", $false)
				$terminate = 1
			}
		}
	}

	if ($terminate -ne 1) {
		if ($db.move_cifs -eq 1 -OR $db.move_sftp -eq 1) {
			Write-Host "`n--------------------------------------------"
			Write-Host " Move File Operations..."
			Write-Host "--------------------------------------------`n"
		}
	}

	if ($terminate -ne 1) {
		if ($db.stopService -eq 1) {
			# Get Service from Remote Machine or log error
			if (($asaService = (getASAService $remoteService $remoteServer)) -eq $false) {
				$errorResults.Add($remoteService,$false)
				$terminate = 1
			} else {
				# Stop service on remote machine and wait for status
				if ((stopAndWaitForService $asaService) -eq $false) {
					$serviceName = $asaService.Name
					$errorResults.Add("Stop $serviceName Service",$false)
					$terminate = 1
				}
			}
		}
	}
	
	if ($terminate -ne 1) {
		if ($db.cleanFiles -eq 1) {
			if ((clean_files $db.destination $options.fileAge) -eq $false) {
				$errorResults.Add("Cleaning Files", $false)
			}
		}
	}

	if ($terminate -ne 1) {
		if ($db.move_cifs -eq 1) {
			if ($db.includeFile) {
				$sourceFile = gi "$workingFolder\$backupName"
				$targetFile = $db.destination
				if ((move_files_filter $sourceFile $targetFile $db.includeFile) -eq $false) {
					$errorResults.Add("Move $sourceFile to $targetFile",$false)
				}
			} else {
				$targetFile = $db.destination
				if ((move_files $compressedFile $targetFile) -eq $false) {
					$errorResults.Add("Move $compressedFile to $targetFile",$false)
				}
			}
		}
	}
	
	if ($terminate -ne 1) {
		if ($db.move_sftp -eq 1) {
			if ($db.includeFile) {
				$sourceFile = gi "$workingFolder\$backupName"
				$targetFile = $hostname
				if ((sftp_files $hostname $username $sourceFile $certfile $db.includeFile) -eq $false) {
					$errorResults.Add("SFTP Transfer $sourceFile to $targetFile",$false)
				}
			} else {
				$sourceFile = gi "$workingFolder\$backupName"
				$fileList = gci "$workingFolder\$backupName" | select -exp FullName
				$targetFile = $hostname
				if ((sftp_files $hostname $username $sourceFile $certfile $fileList) -eq $false) {
					$errorResults.Add("SFTP Transfer $sourceFile to $targetFile",$false)
				}
			}
		}
	}

	if ($terminate -ne 1) {
		if ($db.stopService -eq 1) {
			# Start service on remote machine and wait for status
			if ((startAndWaitForService $asaService) -eq $false) {
				$serviceName = $asaService.Name
				$errorResults.Add("Start $serviceName Service",$false)
			}
		}
	}
	
	#rm -force -recurse $workingfolder

	Write-Host "`n--------------------------------------------"
	Write-Host "Error Results..."
	Write-Host "--------------------------------------------`n"
	$errorSubjectLine = $false
	if ($terminate -eq 1) {
		write-host "Terminating Error Occurred!"
		$errorSubjectLine = $true
	}
	$errorResults.GetEnumerator() | %{
		if ($_.Value -eq $false) {
			$errorName = $_.Name
			Write-Host "$errorName Failed!"
			$errorSubjectLine = $true
		}	
	}
	if ($errorSubjectLine -eq $false) {
		write-host "No Errors.  Yipee!"
	}

	write-host "`n`n"

	Stop-Transcript

	[string]$from = $email.email.sender

	[string[]]$to = $email.email.recipients

	if ($errorSubjectLine -eq $true) {
		[string]$subject = "$Server FAILED! Database Maintenance Tasks"
	} else {
		[string]$subject = "$Server PASSED! Database Maintenance Tasks"
	}

	[string]$body = (gc $log) | out-string

	send-mailmessage -from $from -to $to -subject $subject -body $body -smtpServer $email.email.smtpServer
}