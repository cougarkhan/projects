;function getASAService {
	param ([string]$serviceName, [string]$serverName)
	
	$asaService = $null
	
	$timeResult = measure-command {
		write-host (log_time)"`tGetting $serviceName from $serverName"
		$asaService = get-service -name $serviceName -computername $serverName
	}
	
	if ($asaService -ne $null -AND ($asaService.GetType()).Name -match "ServiceController") {
		write-host (log_time)"`tCompleted getting $serviceName from $serverName. $timeResult"
		return $asaService
	} else {
		write-host (log_time)"`tFailed to get $serviceName from $serverName. $timeResult"
		return $false
	}
}

function stopAndWaitForService {
	param ([System.ServiceProcess.ServiceController]$service)
	
	$serviceName = $service.Name
	$serviceMachine = $service.MachineName
	$timeout = new-timespan -seconds 120
	
	# Stop service on destination machine
	$timeResult = measure-command {
		write-host (log_time)"`tStopping $serviceName on $serviceMachine..."
		write-host (log_time)"`t$serviceName status is: " -nonewline
		$service.stop()
		try {
			$service.waitforstatus([system.ServiceProcess.ServiceControllerStatus]::Stopped, $timeout)
		} catch [System.ServiceProcess.TimeoutException] {
			write-host "`n"
			write-host (log_time)"`tTimeout Exceeded before Service Stopped.  The service may still have stopped."
		} finally {
			$service.refresh()
			$status = $service.status
			write-host "$status"
		}			
	}
	
	write-host (log_time)"`tCompleted Stopping Service. $timeResult"
	
	if ($service.status -eq [system.ServiceProcess.ServiceControllerStatus]::Stopped) {
		return $true
	} else {
		return $false
	}
}

function startAndWaitForService {
	param ([System.ServiceProcess.ServiceController]$service)
	
	$serviceName = $service.Name
	$serviceMachine = $service.MachineName
	$timeout = new-timespan -seconds 120
	
	# Stop service on destination machine
	$timeResult = measure-command {
		write-host (log_time)"`tStarting $serviceName on $serviceMachine..."
		$service.start()
		write-host (log_time)"`t$serviceName status is: " -nonewline
		try {
			$service.waitforstatus([system.ServiceProcess.ServiceControllerStatus]::Running, $timeout)
		} catch [System.ServiceProcess.TimeoutException] {
			write-host "`n"
			write-host (log_time)"`tTimeout Exceeded before Service Started.  The service may still have started."
		} finally {
			$service.refresh()
			$status = $service.status
			write-host "$status"
		}
	}
	
	write-host (log_time)"`tCompleted Starting Service. $timeResult"
	
	if ($service.status -eq [system.ServiceProcess.ServiceControllerStatus]::Running) {
		return $true
	} else {
		return $false
	}
}

function backupScanNetDB {
	param ([string]$pathToPWFile, 
			[string]$pathToKeyFile, 
			[string]$workingFolder, 
			[string]$dbName, 
			[string]$backupEXE
			)
	
	[string[]]$backupARGS = ("-dbpassword",
								(decryptFile $pathToPWFile $pathToKeyFile ),
								"-destination",
								"$workingFolder",
								"-$dbName",
								"-nocompress",
								"-go",
								"-close")
	
	write-host (log_time)"`tBegining Database Backup function for $dbName."
	$timeResult = measure-command {
		start-process -filepath $backupEXE -argumentlist $backupARGS -wait -nonewwindow
		sleep 3
	}
	
	write-host (log_time)"`tCompleted Database Backup function for $dbName. $timeResult"


	if ($backupFolder = (ls $workingFolder -Filter "ScanNetBackup*" | where { ($_.creationtime).toshortdatestring() -eq (date).toshortdatestring() } )) {
		write-host (log_time)"`t$dbname Backup File for today exists."
		return $backupFolder
	} else {
		write-host (log_time)"`t$dbname Backup File for today cannot be found!"
		return $false
	}
}

function backupMicrokeyDB {
	param ([string]$pathToPWFile, 
			[string]$pathToKeyFile,
			[string]$workingFolder,
			[string]$adminName,
			[string]$dbName, 
			[string]$backupEXE,
			[string]$backupFolder
			)
			
	$pwItem = (decryptFile $pathToPWFile $pathToKeyFile)
			
	[string[]]$backupARGS = ("-c",
								"uid=$adminName;pwd=$pwItem;dsn=$dbName",
								"$backupFolder",
								"-x",
								"-y")
	
	write-host (log_time)"`tBegining Database Backup function for $dbName."
	$timeResult = measure-command {
		start-process -filepath $backupEXE -argumentlist $backupARGS -wait -nonewwindow
		sleep 3
	}
	
	write-host (log_time)"`tCompleted Database Backup function for $dbName. $timeResult"


	if ($backupFolderItem = gi $backupFolder) {
		write-host (log_time)"`t$dbname Backup File for today exists."
		return $backupFolderItem
	} else {
		write-host (log_time)"`t$dbname Backup File for today cannot be found!"
		return $false
	}
}

function truncate_log {
	param ([string]$pathToPWFile, 
			[string]$pathToKeyFile,
			[string]$adminName,
			[string]$dbName,
			[string]$truncateEXE
			)
			
	$pwItem = (decryptFile $pathToPWFile $pathToKeyFile)
			
	[string[]]$truncateARGS = ("-c",
								"uid=$adminName;pwd=$pwItem;dsn=$dbName",
								"-xo")
	
	write-host (log_time)"`tBegining Database Log Truncate function for $dbName."
	$timeResult = measure-command {
		start-process -filepath $truncateEXE -argumentlist $truncateARGS -wait -nonewwindow
	}
	write-host (log_time)"`tCompleted Database Log Truncate function for $dbName. $timeResult"
}