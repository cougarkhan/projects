#####################################################################################
#
# Version 1.05
#
# Written on 12/06/2012
# By Michael Pal
# 
#####################################################################################



function create_log {
	param ([string]$logPath)
	
	if (!(Test-Path $logPath)) {
		Write-Host (log_time)"`tCreating new log at $logPath"
		$log = New-Item -Path $logPath -type File -Force
	} else {
		Write-Host (log_time)"`tRetrieving log file " -nonewline
		Write-Host "$logPath" -nonewline -foregroundcolor "CYAN"
		Write-Host "..." -nonewline
		
		$log = (gi $logPath)
	}
	
	return $log
}

function clean_files {
	param($folder, $ageDays)
	
	$result = $true
	If (Test-Path $folder) {
		
		$fileAgeLimit = (get-date).AddDays(-$ageDays)
		
		$filesCleaned = 0
		if (($files = ls $folder) -ne $null) {
			$files | %{ 
				if ($_.CreationTime -lt $fileAgeLimit) {
					if (test-path $_.FullName) {
						Write-Host (log_time)"`tRemoving old file"$_.FullName"..." -nonewline
						$timeResult = measure-command { 
							Set-ItemProperty -Path $_.FullName -Name IsReadOnly -value $false
							Remove-Item -Path $_.FullName 
						}
						if (!(test-path $_.FullName)) {
							Write-Host (log_time)"`tCompleted. $timeResult"
						} else {
							Write-Host (log_time)"`tFAILED!. $timeResult"
							$result = $false
						}
						$filesCleaned++
					}
				}
			}
		}
		if ($filesCleaned -lt 1) {
			Write-Host (log_time)"`t$folder Has no files old enough to clean up.`n"
		}
	} Else {
		Write-Host (logtime)"`t$folder Does not exist.  This may be the first run of the backup."
	}
	
	return $result
}

function move_files_filter {
	param ($source, [string]$target, [string[]]$filter)
	
	$timeResult = measure-command {
		foreach ($item in $filter) {
			Write-Host (log_time)"`tMoving $item to $target..." -nonewline
			$file = gci $source.fullname -filter $item -recurse -force
			cp $file.fullname $target
			write-host "Completed."
			$name = $file.name
			write-host (log_time) "`tChecking if file $target$name exists..." -nonewline
			if (test-path "$target$name") {
				write-host "Success."
				$result = $true
			} else {
				write-host "Failure."
				$result = $false
			}
		}
	}
	
	write-host (log_time)"`tMove files Completed in $timeResult."
	
	write-host (log_time)"`tRemoving $source..." -nonewline
	rm $source -recurse -force
	write-host "Completed."
	
	return $result
}

function move_files {
	param ($source, [string]$target)
	
	$timeResult = measure-command {
		Write-Host (log_time)"`tMoving $source to $target..." -nonewline
		cp $source.fullname $target -recurse -force
		write-host "Completed."
		write-host (log_time) "`tChecking if file exists..." -nonewline
		$name = $source.name
		if (test-path "$target$name") {
			write-host "Success."
			$result = $true
		} else {
			write-host "Failure."
			$result = $false
		}
	}
	
	write-host (log_time)"`tMove files Completed in $timeResult."
	
	write-host (log_time)"`tRemoving $source..." -nonewline
	rm $source -recurse -force
	write-host "Completed."
	
	return $result
}

function sftp_files {
	param([string]$server, [string]$username, [string]$source_dir, [string]$path_to_key_file, [string[]]$file_list)
	$winscp_arguments = "/console /log=C:\DRS\Logs\winscp.log /privatekey=$path_to_key_file"
	
	$winscp_exe = "C:\Program Files (x86)\WinSCP\WinSCP.com"
	$winscp = new-object System.Diagnostics.Process
	$winscp.StartInfo.FileName = $winscp_exe
	$winscp.StartInfo.Arguments = $winscp_arguments
	$winscp.StartInfo.UseShellExecute = $false;
	$winscp.StartInfo.RedirectStandardInput = $true;
	$winscp.StartInfo.RedirectStandardOutput = $true;
	$winscp.StartInfo.CreateNoWindow = $true;
	$winscp.Start();
		
	$winscp_script_file = @(
	"option batch on",
	"option confirm off",
	"open sftp://$username@$server",
	"option transfer binary",
	"lcd $source_dir"
	)
	
	foreach ($file in $file_list) {
		$winscp_script_file += "put $file"
	}
	$winscp_script_file += "close"
	$winscp_script_file += "exit"
	
	foreach ($command in $winscp_script_file) {
		$winscp.StandardInput.WriteLine($command)
	}
	$winscp.StandardInput.Close()
	$output = $winscp.StandardOutput.ReadToEnd()
	
	$winscp.WaitForExit()
	
	if ($winscp.ExitCode -eq 0)
	{
		return $true
	} else {
		return $false
	}
	
	$output
}

function compress {
	param ([string]$source, [string]$7zipEXE)
	
	$source | %{ 
	
		$sourceFile = $_
		$targetFile = $sourceFile + ".7z"
		
		[string[]]$7zipARGS = @("a",$targetFile,$sourceFile,"-mx=3","-m0=lzma2:d26","-mmt=1")
		write-host (log_time)"`tCompressing $sourceFile to $targetFile"
		
		$timeResult = measure-command {
			start-process -filepath $7zipEXE -argumentlist $7zipARGS -wait -nonewwindow
		}
		
		write-host (log_time)"`tCompleted compressing $targetFile in $timeResult"
		
		if (test-path $targetFile) {
			write-host (log_time)"`tRemoving $sourceFile"
			rm $SourceFile -recurse -force
			return $targetFile
		} else {
			write-host (log_time)"`t$targetFile does not exist!"
			return $false
		}
	}
}

function mapOpDrive {
	param([string]$root, [string]$name)
	
	Write-Host (log_time)"`tMapping $root to $name"
	$timeResult = measure-command {
		$psd = new-psdrive -root $root -Name $name -scope "Script" -PSprovider FileSystem
	}
	
	Write-Host (log_time)"`tCompleted mapping $root to $name in $timeResult"
	
	if ($psd.name -eq $name) {
		return $true
	} else {
		return $false
	}
}